//action types
export const SET_JSON_RESULTS = "SET_JSON_RESULTS";

//action creators
export function setJsonResults(jsonResults) {
    return {
        type: SET_JSON_RESULTS,
        jsonResults,
    }
}