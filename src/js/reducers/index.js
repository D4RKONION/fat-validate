import { combineReducers } from 'redux'
import jsonResults from './jsonresults'

export default combineReducers({
  jsonResults,
})