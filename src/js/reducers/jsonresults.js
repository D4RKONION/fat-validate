export default function jsonResults(state = {}, action) {
  switch (action.type) {
    case "SET_JSON_RESULTS":
      return action.jsonResults
    default:
      return state
  }
}