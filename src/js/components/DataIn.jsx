import styles from "../../style/components/DataIn.module.scss";

const DataIn = ({ value, onChange }) => {

	return (
		<div id={styles.DataIn}>
			<h1>Let's Get Started!</h1>
			<h2>Please paste the JSON you got from the sheet in here</h2>
			<textarea
				value={value}
				onChange={onChange}
			></textarea>
		</div>
	)
}

export default DataIn;