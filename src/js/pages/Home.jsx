import { useNavigate } from 'react-router-dom';

const Home = () => {
  let navigate = useNavigate();

  const modeTypes = [
    "SFVFrameData",
    "SFVSpecificCancels",
    "SF6FrameData",
    "GGSTFrameData",
  ]


  return (
    <div className="home">
      <h1>FAT Validate</h1>
      <h2>Please Choose a Validation Type</h2>

      <div style={{display:'flex', flexWrap: "wrap", width: "50%", margin: "0 auto", justifyContent: "center"}}>
        {modeTypes.map(modeName =>
          <button
            style={{ height: "100px", margin: "7px", fontSize: "20px", borderRadius: "20px", flex: "0 0 250px"}}
            key={`Homepage-button-${modeName}`}
            className="pure-u-1-2 pure-button pure-button-primary"
            onClick={() => navigate(`/${modeName}/Input`)}
          >{modeName}</button>
        )}
      </div>
      <p>Ver 1.12.0</p>
      <p>Last Updated: Feb 05, 2025</p>
    </div>
  )

}

export default Home;