import { useEffect, useState } from "react"
import { useSelector } from "react-redux"
import { useParams } from "react-router"

const Success = () => {

  const jsonResults = useSelector(state => state.jsonResults)
  const [ serverVersionDetails, setServerVersionDetails ] = useState({})
  const modeSlug = useParams().modeSlug;

  useEffect(() => {
    const callServer = async () => {
      const gameName = modeSlug === "SFVFrameData" ? "SFV" : modeSlug === "GGSTFrameData" ? "GGST" : modeSlug === "SF6FrameData" ? "SF6" : "uh-oh"
      const versionResponse = await fetch(`https://fullmeter.com/fatfiles/release/${gameName}/FrameData/${gameName}FrameDataVersionDetails.json?ts=${Date.now()}`)
      const serverVersionDetailsJson = await versionResponse.json();

      setServerVersionDetails(serverVersionDetailsJson)
    }
    
    callServer();
  }, [])

  const writeToClipboard = (whatToWrite) => {
    console.log(whatToWrite)
    if (whatToWrite === "frames") {
      navigator.clipboard.writeText(JSON.stringify(jsonResults))
    } else if (whatToWrite === "version") {

      const dateObj = new Date();

      const newdate = dateObj.toDateString().substring(4);

      navigator.clipboard.writeText(JSON.stringify(
        {
          "VERSION_CODE": serverVersionDetails.VERSION_CODE + 1,
          "MINIMUM_VERSION_REQUIRED": serverVersionDetails.MINIMUM_VERSION_REQUIRED,
          "DATE_UPDATED": newdate
        }
      , null, 2))
    }
  }

  return (
    <div className="success">
      <h1>{modeSlug}</h1>
      <h1>Success!</h1>
      <h2>Click here to copy the json to your clipboard</h2>
      <button className="pure-button pure-button-primary" onClick={() => writeToClipboard("frames")}>Copy JSON</button>
      
      {modeSlug === "SFVFrameData" || modeSlug === "GGSTFrameData" || modeSlug === "SF6FrameData"  ?
        <>
        <hr style={{width: "50%", maxWidth: "700px", margin: "20px auto"}}></hr>
        {serverVersionDetails.VERSION_CODE ?
          <>
           <h2>Once you've pasted that somewhere,<br/> click here to copy the version details you need</h2>
           <button className="pure-button pure-button-primary" onClick={() => writeToClipboard("version")}>Copy Version Details</button>
           <h3>Please make sure that there are 3 lines in the Version details:<br />VERSION_CODE<br />MINIMUM_VERSION_REQUIRED<br />DATE_UPDATED</h3>
          </>
        : <h2>Just a moment, fetching the current version details from the server</h2>
        
        }
        </>
      : <h1>You shouldn't be here Adam/JoJo {">:("}</h1>
      
      }
     
    </div>
  )
}

export default Success;