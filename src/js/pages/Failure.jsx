import { useSelector } from "react-redux";

const Failure = () => {

  const jsonResults = useSelector(state => state.jsonResults)

  return (
    <div className="failure">
      <h1>You messed up...</h1>

      <h2>{jsonResults.errChar} ({jsonResults.errState})</h2>
      <h2>{jsonResults.errMove}</h2>
      <h3>{jsonResults.errEntry}</h3>
      {jsonResults.err &&
        <h3>{jsonResults.err}</h3>
      }
    </div>
  )
}

export default Failure;