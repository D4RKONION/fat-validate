import { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router";
import { setJsonResults } from "../actions";

import DataIn from '../components/DataIn'


const Input = () => {
  let navigate = useNavigate();
  const modeSlug = useParams().modeSlug;
  const dispatch = useDispatch();

  const [rawJSONString, setRawJSONString] = useState("");

  const handleRawJSONString = (event) => {
    const rawFrameDataJSON = JSON.parse(event.target.value) 
    setRawJSONString(event.target.value);
      
    let JSONValidatorWorker = new Worker(`./workers/${modeSlug}.js`);
    JSONValidatorWorker.postMessage(rawFrameDataJSON);

    JSONValidatorWorker.onmessage = function(e) {
      dispatch(setJsonResults(e.data[1]))
      if (e.data[0] === "success") {
        navigate(`/${modeSlug}/Success`)
      }
      else {
        navigate(`/${modeSlug}/Failure`)
      }
    }
  }
  
  return (
    <div className="input">
       <h1>{modeSlug}</h1>
        <DataIn
          value={rawJSONString}
          onChange={handleRawJSONString}
        />
    </div>
   
  )
}
export default Input;