import './App.css';
import React from "react";
import {
	HashRouter as Router,
	Route,
  Routes,
} from "react-router-dom";

import Home from './js/pages/Home';
import Input from './js/pages/Input';
import Success from './js/pages/Success';
import Failure from './js/pages/Failure';

const App = () => {
	return (
		<div className="App">
			<Router>
				<Routes>
					<Route path={`/:modeSlug/input`} element={<Input />} />
          <Route path={`/:modeSlug/Success`} element={<Success />}/>
          <Route path={`/:modeSlug/Failure`} element={<Failure />} />

					<Route path={`/`} element={<Home />} />
				</Routes>					
			</Router>        
		</div>
	)
};

export default App;
