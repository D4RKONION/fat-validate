function convertSheetJSONtoFATJSONGGST(rawData) {
	const characterList = ["ABA", "Anji", "Asuka", "Axl", "Baiken", "Bedman", "Bridget", "Chipp", "Dizzy", "Elphelt", "Faust", "Giovanna", "Goldlewis", "H. Chaos",  "I-No", "Jack-O", "Johnny", "Ky", "Leo", "May", "Millia", "Nagoriyuki", "Potemkin", "Ramlethal", "Sin", "Slayer", "Sol", "Testament", "Zato-1"]

// const characterList = ["Zato-1"]

	const ggstData = {}
  const ignoreRows = [];
	let errChar = "";
	let errState = "";
	let errMove = "";
	let errEntry = "";

	// 2021 note
	// this is the same function I've been using for years
	// it is arcane magic and I'm not touching it.
	// I'm sorry
	// But not sorry enough to change it
	try {
		for (var character in characterList) {
			let charName = characterList[character];
			errChar = characterList[character];
			ggstData[charName] = {"moves": {"normal": {}}, "stats": {}};
			
			let workingDataNormal = rawData[charName + "Normal"];
			errState = "Normal"

			for(var move in workingDataNormal) {
					var moveData = workingDataNormal[move];
					errMove = moveData["moveName"]
					
					//creates the new move entry
					ggstData[charName]["moves"]["normal"][moveData["moveName"]] = {};
					
					for (var heading in moveData) {
						errEntry = heading;
						if (!ignoreRows.includes(heading)) {
								var preFormatData = moveData[heading];
								//before starting, check that none of the entites start with whitespace. If it does, throw an error
								if (preFormatData[0] === " ") {
									throw `whitespace detected in the first character of this cell for ${charName}: ${moveData["moveName"]}'s ${heading}`;
								}
								// First we skip over any blank entries. These can be accomidated for in the app, and significantly cut down on JSON space
								if (preFormatData !== "" && preFormatData !== "~" && preFormatData !== "-" && preFormatData !== "false") {
										// Next we check whether the entry is a number or not. If it is, we go to 'else' and Number() it
										if (isNaN(preFormatData) || heading === "numCmd") {
											
											if (preFormatData === "true" || preFormatData[0] === "{" || (preFormatData[0] === "[" && (heading === "extraInfo" || heading === "xx" || heading === "gatling" || heading === "multiActive" ) )) {
												// If entry is a Boolean, or a JSON or a list, we need to parse it	
												ggstData[charName]["moves"]["normal"][moveData["moveName"]][heading] = JSON.parse(preFormatData)
											} else if (heading === "numCmd") {
												// some numCmd entries are just numbers. This breaks things. We need to make sure it's a string
												ggstData[charName]["moves"]["normal"][moveData["moveName"]][heading] = preFormatData.toString();
											} else {
												ggstData[charName]["moves"]["normal"][moveData["moveName"]][heading] = preFormatData;												
											}
										} else {
												ggstData[charName]["moves"]["normal"][moveData["moveName"]][heading] = Number(preFormatData);
										}
								}
						}
					}

					if (!ggstData[charName]["moves"]["normal"][moveData["moveName"]].dustloopKey) {
						
						if (moveData["moveName"] === "Throw") {
							ggstData[charName]["moves"]["normal"][moveData["moveName"]].dustloopKey = "Ground_Throw"
						} else if (
							moveData["moveName"].substring(0, 2) === "P " ||
							moveData["moveName"].substring(0, 2) === "K " ||
							moveData["moveName"].substring(0, 2) === "S " ||
							moveData["moveName"].substring(0, 2) === "H " ||
							moveData["moveName"].substring(0, 2) === "D "
						) {
							ggstData[charName]["moves"]["normal"][moveData["moveName"]].dustloopKey = moveData["moveName"].slice(2).replaceAll(" ", "_")
							if (moveData["moveName"].includes(" (air)")) {
								ggstData[charName]["moves"]["normal"][moveData["moveName"]].dustloopKey = moveData["moveName"].replaceAll(" (air)", "").replaceAll(" ", "_")
							}
						} else if (moveData["moveName"].includes(" (air)")) {
							ggstData[charName]["moves"]["normal"][moveData["moveName"]].dustloopKey = moveData["moveName"].replaceAll(" (air)", "").replaceAll(" ", "_")
						} else {
							ggstData[charName]["moves"]["normal"][moveData["moveName"]].dustloopKey = moveData["moveName"].replaceAll(" ", "_")
						}			
					}
			}


			// check if this character has a special state that needs to be parsed
			Object.keys(rawData).forEach(keyName => {
				if (keyName.includes(charName) && keyName !== `${charName}Normal` && keyName !== `${charName}Stats`) {
					let workingDataSpecialState = rawData[keyName];
					const stateName = keyName.substring(charName.length);
					errState = stateName;

					ggstData[charName]["moves"][stateName] = {};

					for (var move in workingDataSpecialState) {
						var moveData = workingDataSpecialState[move];
						errMove = moveData["moveName"]

						//creates the new move entry
						ggstData[charName]["moves"][stateName][moveData["moveName"]] = {};
						
						for (heading in moveData) {
							errEntry = heading;
		
							if (!ignoreRows.includes(heading)) {
								var preFormatData = moveData[heading];
								//before starting, check that none of the entites start with whitespace. If it does, throw an error
								if (preFormatData[0] === " ") {
									throw `whitespace detected in the first character of this cell for ${charName}: ${moveData["moveName"]}'s ${heading}`;
								}
								// First we skip over any blank entries. These can be accomidated for in the app, and significantly cut down on JSON space
								if (preFormatData !== "" && preFormatData !== "~" && preFormatData !== "-" && preFormatData !== "false") {
										// Next we check whether the entry is a number or not. If it is, we go to 'else' and Number() it
										if (isNaN(preFormatData) || heading === "numCmd") {
											
											if (preFormatData === "true" || preFormatData[0] === "{" || (preFormatData[0] === "[" && !heading.includes("Cmd") && !heading.includes("Name"))) {
												// If entry is a Boolean, or a JSON or a list, we need to parse it	
												ggstData[charName]["moves"][stateName][moveData["moveName"]][heading] = JSON.parse(preFormatData)
											} else if (heading === "i" && preFormatData === "r") {
												//if the index column says r, we should make it the normal's index instead
												ggstData[charName]["moves"][stateName][moveData["moveName"]][heading] = ggstData[charName]["moves"]["normal"][moveData["moveName"]][heading];
											} else if (heading === "numCmd") {
												// some numCmd entries are just numbers. This breaks things. We need to make sure it's a string
												ggstData[charName]["moves"][stateName][moveData["moveName"]][heading] = preFormatData.toString();
											} else {
												ggstData[charName]["moves"][stateName][moveData["moveName"]][heading] = preFormatData;												
											}
										} else {
												ggstData[charName]["moves"][stateName][moveData["moveName"]][heading] = Number(preFormatData);
										}
								}
						}
						}

						if (!ggstData[charName]["moves"][stateName][moveData["moveName"]].dustloopKey) {
						
							if (moveData["moveName"] === "Throw") {
								ggstData[charName]["moves"][stateName][moveData["moveName"]].dustloopKey = "Ground_Throw"
							} else if (
								moveData["moveName"].substring(0, 2) === "P " ||
								moveData["moveName"].substring(0, 2) === "K " ||
								moveData["moveName"].substring(0, 2) === "S " ||
								moveData["moveName"].substring(0, 2) === "H " ||
								moveData["moveName"].substring(0, 2) === "D "
							) {
								ggstData[charName]["moves"][stateName][moveData["moveName"]].dustloopKey = moveData["moveName"].slice(2).replaceAll(" ", "_")
								if (moveData["moveName"].includes(" (air)")) {
									ggstData[charName]["moves"][stateName][moveData["moveName"]].dustloopKey = moveData["moveName"].replaceAll(" (air)", "").replaceAll(" ", "_")
								}
							} else if (moveData["moveName"].includes(" (air)")) {
								ggstData[charName]["moves"][stateName][moveData["moveName"]].dustloopKey = moveData["moveName"].replaceAll(" (air)", "").replaceAll(" ", "_")
							} else {
								ggstData[charName]["moves"][stateName][moveData["moveName"]].dustloopKey = moveData["moveName"].replaceAll(" ", "_")
							}			
						}
				}




				}
			})
	
			//stats
			errState = "Stats"
			if (typeof rawData[charName + "Stats"] !== "undefined") {
					var workingDataStats = rawData[charName + "Stats"];
					
					//incorporate the sheet stats
					for (var entry in workingDataStats) {
							ggstData[charName]["stats"][workingDataStats[entry]["name"]] = workingDataStats[entry]["stat"].toString();
					}
					
					//create a fastest normal stat
					let currentFastestNormal = [10, "moveName"];
					
          rawData[charName + "Normal"].map(moveData => {
            if (moveData.moveType === "normal" && moveData.followUp !== "true" && moveData.airmove !== "true" && !moveData.movesList && parseInt(moveData.startup) < currentFastestNormal[0]) {
              currentFastestNormal = [moveData.startup, moveData.moveName]
            }
          })
					
					ggstData[charName]["stats"]["fastestNormal"] = `${currentFastestNormal[0].toString()}f`;

			}
		}
		console.log(ggstData)
    return [
			"success",
			ggstData
		];

	} catch(err) {
		console.log(err)
		return [
			"error",
			{ errChar, errState, errMove, errEntry }
		];
	}
	



    

}

onmessage = function(e) {
	const fatFormattedJSON = convertSheetJSONtoFATJSONGGST(e.data);
	postMessage(fatFormattedJSON)
}
