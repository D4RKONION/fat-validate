function createFATSpecificCancelsArray(rawData) {
  console.log(rawData)
  try {
    let finalArray = [];

    for (let x=0; x <= rawData[(rawData.length - 1)].rowNumber; x++) {
      finalArray.push({})
      for (dataEntry in rawData) {
        if(rawData[dataEntry].rowNumber === x) {
          tempKey = rawData[dataEntry].dataFileKey;
          finalArray[x][tempKey] = {
            dataTableHeader: rawData[dataEntry].dataTableHeader,
            detailedHeader: rawData[dataEntry].detailedHeader,
            dataFileKey: rawData[dataEntry].dataFileKey,
            usedBy: rawData[dataEntry].usedBy
          }
        }	
      }
    }
    console.log(finalArray)

    return [
			"success",
			finalArray
		];

	} catch(err) {
		return [
			"error",
			{}
		];
	}


}

onmessage = function(e) {
	const fatSpecificCancelsArray = createFATSpecificCancelsArray(e.data);
	postMessage(fatSpecificCancelsArray)
}
