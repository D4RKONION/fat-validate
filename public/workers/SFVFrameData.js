function convertSheetJSONtoFATJSON(rawData) {
	const characterList = ["Abigail", "Akira", "Akuma", "Alex", "Balrog", "Birdie", "Blanka", "Cammy", "Chun-Li", "Cody", "Dan", "Dhalsim", "E.Honda", "Ed", "Falke", "F.A.N.G", "G", "Gill", "Guile", "Ibuki", "Juri", "Kage", "Karin", "Ken", "Kolin", "Laura", "Lucia", "Luke", "M.Bison", "Menat", "Nash", "Necalli", "Oro", "Poison", "Rashid", "R.Mika", "Rose", "Ryu", "Sagat", "Sakura", "Seth", "Urien", "Vega", "Zangief", "Zeku (Old)", "Zeku (Young)"]

	const sfvData = {}
  const ignoreRows = ["hitstop", "meterGain", "CH Damage", "CH Stun", "chip"];
	let errChar = "";
	let errState = "";
	let errMove = "";
	let errEntry = "";

	// 2021 note
	// this is the same function I've been using for years
	// it is arcane magic and I'm not touching it.
	// I'm sorry
	// But not sorry enough to change it
	try {
		for (character in characterList) {
			let charName = characterList[character];
			errChar = characterList[character];
			
			sfvData[charName] = {"moves": {"normal": {}, "vtOne": {}, "vtTwo": {}}, "stats": {}};
			
			let workingDataNormal = rawData[charName + "Normal"];
			errState = "Normal"

			for(var move in workingDataNormal) {
					var moveData = workingDataNormal[move];
					errMove = moveData["moveName"]
					
					//creates the new move entry
					sfvData[charName]["moves"]["normal"][moveData["moveName"]] = {};
					
					for (heading in moveData) {
						errEntry = heading;
	
						if (!ignoreRows.includes(heading)) {
								var preFormatData = moveData[heading];
								//before starting, check that none of the entites start with whitespace. If it does, throw an error
								if (preFormatData[0] === " ") {
									throw `whitespace detected in the first character of this cell for ${charName}: ${moveData["moveName"]}'s ${heading}`;
								}
								// First we skip over any blank entries. These can be accomidated for in the app, and significantly cut down on JSON space
								if (preFormatData !== "" && preFormatData !== "~" && preFormatData !== "-" && preFormatData !== "false") {
										// Next we check whether the entry is a number or not. If it is, we go to 'else' and Number() it
										if (isNaN(preFormatData) || heading === "numCmd") {
											// if entry is ccAdv we need to break it into it's three separate parts
											if (heading === "crushAdv") {
												sfvData[charName]["moves"]["normal"][moveData["moveName"]]["ccState"] = JSON.parse(preFormatData).state;
												sfvData[charName]["moves"]["normal"][moveData["moveName"]]["ccAdv"] = JSON.parse(preFormatData).ccAdv;
												sfvData[charName]["moves"]["normal"][moveData["moveName"]]["ccVG"] = JSON.parse(preFormatData).vG;
											} else if (preFormatData === "true" || preFormatData[0] === "{" || (preFormatData[0] === "[" && heading !== "cmnName")) {
												// If entry is a Boolean, or a JSON or a list, we need to parse it	
												sfvData[charName]["moves"]["normal"][moveData["moveName"]][heading] = JSON.parse(preFormatData)
											} else if (heading === "numCmd") {
												// some numCmd entries are just numbers. This breaks things. We need to make sure it's a string
												sfvData[charName]["moves"]["normal"][moveData["moveName"]][heading] = preFormatData.toString();
											} else {
												sfvData[charName]["moves"]["normal"][moveData["moveName"]][heading] = preFormatData;												
											}
										} else {
												sfvData[charName]["moves"]["normal"][moveData["moveName"]][heading] = Number(preFormatData);
										}
								}
						}
					}
			}
	
			errState = "V-Trigger 1"
			if (typeof rawData[charName + "Trigger1"] !== "undefined") {
					var workingDataTrigger1 = rawData[charName + "Trigger1"];
					for(var move in workingDataTrigger1) {
							var moveData = workingDataTrigger1[move];
							errMove = moveData["moveName"];
							
							//creates the new move entry
							sfvData[charName]["moves"]["vtOne"][moveData["moveName"]] = {};
							
							for (heading in moveData) {
								errEntry = heading;
								
								if (!ignoreRows.includes(heading)) {
										var preFormatData = moveData[heading];
										//before starting, check that none of the entites start with whitespace. If it does, throw an error
										if (preFormatData[0] === " ") {
											throw `whitespace detected in the first character of this cell for ${charName}: ${moveData["moveName"]}'s ${heading}`;
										}
										// First we skip over any blank entries. These can be accomidated for in the app, and significantly cut down on JSON space
										if (preFormatData !== "" && preFormatData !== "~" && preFormatData !== "-" && preFormatData !== "false") {
												// Next we check whether the entry is a number or not. If it is, we go to 'else' and Number() it
												if (isNaN(preFormatData) || heading === "numCmd") {
													if (heading === "crushAdv") {
													sfvData[charName]["moves"]["vtOne"][moveData["moveName"]]["ccState"] = JSON.parse(preFormatData).state;
													sfvData[charName]["moves"]["vtOne"][moveData["moveName"]]["ccAdv"] = JSON.parse(preFormatData).ccAdv;
													sfvData[charName]["moves"]["vtOne"][moveData["moveName"]]["ccVG"] = JSON.parse(preFormatData).vG;
												}
														// If entry is a Boolean, or a JSON or a list, we need to parse it
														else if (preFormatData === "true" || preFormatData[0] === "{" || preFormatData[0] === "[" && heading !== "cmnName") {
															sfvData[charName]["moves"]["vtOne"][moveData["moveName"]][heading] = JSON.parse(preFormatData)
														} else if (heading === "i" && preFormatData === "r") {
															//if the index column says r, we should make it the normal's index instead
															sfvData[charName]["moves"]["vtOne"][moveData["moveName"]][heading] = sfvData[charName]["moves"]["normal"][moveData["moveName"]][heading];
														} else if (heading === "numCmd") {
															// some numCmd entries are just numbers. This breaks things. We need to make sure it's a string
															sfvData[charName]["moves"]["vtOne"][moveData["moveName"]][heading] = preFormatData.toString();
														} else {
															sfvData[charName]["moves"]["vtOne"][moveData["moveName"]][heading] = preFormatData;
														}
												} else {
														sfvData[charName]["moves"]["vtOne"][moveData["moveName"]][heading] = Number(preFormatData);
												}
										}
								}
						}
					}
			}
			
			errState = "V-Trigger 2"
			if (typeof rawData[charName + "Trigger2"] !== "undefined") {
					var workingDataTrigger2 = rawData[charName + "Trigger2"];
					for(var move in workingDataTrigger2) {
							var moveData = workingDataTrigger2[move];
							errMove = moveData["moveName"]
							
							//creates the new move entry
							sfvData[charName]["moves"]["vtTwo"][moveData["moveName"]] = {};
							
							for (heading in moveData) {
								errEntry = heading;

								if (!ignoreRows.includes(heading)) {
										var preFormatData = moveData[heading];
										//before starting, check that none of the entites start with whitespace. If it does, throw an error
										if (preFormatData[0] === " ") {
											throw `whitespace detected in the first character of this cell for ${charName}: ${moveData["moveName"]}'s ${heading}`;
										}
										// First we skip over any blank entries. These can be accomidated for in the app, and significantly cut down on JSON space
										if (preFormatData !== "" && preFormatData !== "~" && preFormatData !== "-" && preFormatData !== "false") {
												// Next we check whether the entry is a number or not. If it is, we go to 'else' and Number() it
												if (isNaN(preFormatData) || heading === "numCmd") {
													if (heading === "crushAdv") {
													sfvData[charName]["moves"]["vtTwo"][moveData["moveName"]]["ccState"] = JSON.parse(preFormatData).state;
													sfvData[charName]["moves"]["vtTwo"][moveData["moveName"]]["ccAdv"] = JSON.parse(preFormatData).ccAdv;
													sfvData[charName]["moves"]["vtTwo"][moveData["moveName"]]["ccVG"] = JSON.parse(preFormatData).vG;
												}
														// If entry is a Boolean, or a JSON or a list, we need to parse it
														else if (preFormatData === "true" || preFormatData[0] === "{" || preFormatData[0] === "[" && heading !== "cmnName") {
																sfvData[charName]["moves"]["vtTwo"][moveData["moveName"]][heading] = JSON.parse(preFormatData)
														} else if (heading === "i" && preFormatData === "r") {
															//if the index column says r, we should make it the normal's index instead
																sfvData[charName]["moves"]["vtTwo"][moveData["moveName"]][heading] = sfvData[charName]["moves"]["normal"][moveData["moveName"]][heading];
														} else if (heading === "numCmd") {
															// some numCmd entries are just numbers. This breaks things. We need to make sure it's a string
															sfvData[charName]["moves"]["vtTwo"][moveData["moveName"]][heading] = preFormatData.toString();
														} else {
															sfvData[charName]["moves"]["vtTwo"][moveData["moveName"]][heading] = preFormatData;
														}
												} else {
														sfvData[charName]["moves"]["vtTwo"][moveData["moveName"]][heading] = Number(preFormatData);
												}
										}
								}
						}
					}
			}
	
			//stats
			errState = "Stats"
			if (typeof rawData[charName + "Stats"] !== "undefined") {
					var workingDataStats = rawData[charName + "Stats"];
					
					//incorporate the sheet stats
					for (entry in workingDataStats) {
							sfvData[charName]["stats"][workingDataStats[entry]["name"]] = workingDataStats[entry]["stat"].toString();
					}
					
					//create a fastest normal stat
					let currentFastestNormal = [10, "moveName"];
					if (charName === "Dhalsim") {
						currentFastestNormal = [4, "Stand LP"]
					} else {
						rawData[charName + "Normal"].map(moveData => {
							if (moveData.moveType === "normal" &&  moveData.followUp !== "true" && !JSON.parse(moveData.airmove) && !moveData.movesList && parseInt(moveData.startup) < currentFastestNormal[0]) {
								currentFastestNormal = [moveData.startup, moveData.moveName]
							}
						})
					}
					sfvData[charName]["stats"]["fastestNormal"] = `${currentFastestNormal[0].toString()}f`;

					// //create the fastest invul stat
					// let currentBestReversal = "moveName";
					// rawData[charName + "Normal"].map(moveData => {
					// 	if (moveData.moveType === "special") {
					// 		JSON.parse(moveData.extraInfo).forEach(entry => {
					// 			if (entry.includes("Fully invincible on frames 1") || entry.includes("Completely invincible on frames 1")) {
					// 				currentBestReversal = moveData.moveName;
					// 			}
					// 		})
					// 	}
					// 	if (currentBestReversal === "moveName") {
					// 		if (moveData.moveType === "super") {
					// 			JSON.parse(moveData.extraInfo).forEach(entry => {
					// 				if (entry.includes("Fully invincible on frames 1") || entry.includes("Completely invincible on frames 1")) {
					// 					currentBestReversal = moveData.moveName;
					// 				}
					// 			})
					// 		}
					// 	}
					// })
					// sfvData[charName]["stats"]["bestReversal"] = `${currentBestReversal}`;
			}
		}
		console.log(sfvData)
    return [
			"success",
			sfvData
		];

	} catch(err) {
		return [
			"error",
			{ errChar, errState, errMove, errEntry }
		];
	}
	



    

}

onmessage = function(e) {
	const fatFormattedJSON = convertSheetJSONtoFATJSON(e.data);
	postMessage(fatFormattedJSON)
}
