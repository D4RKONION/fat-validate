function convertSheetJSONtoFATJSONSF6(rawData) {
	const characterList = ["A.K.I.", "Akuma", "Blanka", "Cammy", "Chun-Li", "Dee Jay", "Dhalsim", "E.Honda", "Ed", "Guile", "Jamie", "JP", "Juri", "Ken", "Kimberly", "Lily", "Luke", "M.Bison", "Mai", "Manon", "Marisa", "Rashid", "Ryu", "Terry", "Zangief"]


	const sf6Data = {}
  const ignoreRows = [];
	let errChar = "";
	let errState = "";
	let errMove = "";
	let errEntry = "";

	// 2021 note
	// this is the same function I've been using for years
	// it is arcane magic and I'm not touching it.
	// I'm sorry
	// But not sorry enough to change it
	try {
		for (character in characterList) {
			let charName = characterList[character];
			errChar = characterList[character];
			sf6Data[charName] = {"moves": {"normal": {}}, "stats": {}};
			
			let workingDataNormal = rawData[charName + "Normal"];
			errState = "Normal"

			for(var move in workingDataNormal) {
					var moveData = workingDataNormal[move];
					errMove = moveData["moveName"]
					
					//creates the new move entry
					sf6Data[charName]["moves"]["normal"][moveData["moveName"]] = {};
					
					for (heading in moveData) {
						errEntry = heading;
						if (!ignoreRows.includes(heading)) {
								var preFormatData = moveData[heading];
								//before starting, check that none of the entites start with whitespace. If it does, throw an error
								if (preFormatData[0] === " ") {
									throw `whitespace detected in the first character of this cell for ${charName}: ${moveData["moveName"]}'s ${heading}`;
								}
								// First we skip over any blank entries. These can be accomidated for in the app, and significantly cut down on JSON space
								if (preFormatData !== "" && preFormatData !== "~" && preFormatData !== "-" && preFormatData !== "false") {
										// Next we check whether the entry is a number or not. If it is, we go to 'else' and Number() it
										if (isNaN(preFormatData) || heading === "numCmd") {
											
											if (preFormatData === "true" || preFormatData[0] === "{" || (preFormatData[0] === "[" && (heading === "extraInfo" || heading === "xx" || heading === "gatling" || heading === "multiActive" ) )) {
												// If entry is a Boolean, or a JSON or a list, we need to parse it	
												sf6Data[charName]["moves"]["normal"][moveData["moveName"]][heading] = JSON.parse(preFormatData)
											} else if (heading === "numCmd") {
												// some numCmd entries are just numbers. This breaks things. We need to make sure it's a string
												sf6Data[charName]["moves"]["normal"][moveData["moveName"]][heading] = preFormatData.toString();
											} else {
												sf6Data[charName]["moves"]["normal"][moveData["moveName"]][heading] = preFormatData;												
											}
										} else {
												sf6Data[charName]["moves"]["normal"][moveData["moveName"]][heading] = Number(preFormatData);
										}
								}
						}
					}

					// if (!sf6Data[charName]["moves"]["normal"][moveData["moveName"]].dustloopKey) {
						
					// 	if (moveData["moveName"] === "Throw") {
					// 		sf6Data[charName]["moves"]["normal"][moveData["moveName"]].dustloopKey = "Ground_Throw"
					// 	} else if (
					// 		moveData["moveName"].substring(0, 2) === "P " ||
					// 		moveData["moveName"].substring(0, 2) === "K " ||
					// 		moveData["moveName"].substring(0, 2) === "S " ||
					// 		moveData["moveName"].substring(0, 2) === "H " ||
					// 		moveData["moveName"].substring(0, 2) === "D "
					// 	) {
					// 		sf6Data[charName]["moves"]["normal"][moveData["moveName"]].dustloopKey = moveData["moveName"].slice(2).replaceAll(" ", "_")
					// 		if (moveData["moveName"].includes(" (air)")) {
					// 			sf6Data[charName]["moves"]["normal"][moveData["moveName"]].dustloopKey = moveData["moveName"].replaceAll(" (air)", "").replaceAll(" ", "_")
					// 		}
					// 	} else if (moveData["moveName"].includes(" (air)")) {
					// 		sf6Data[charName]["moves"]["normal"][moveData["moveName"]].dustloopKey = moveData["moveName"].replaceAll(" (air)", "").replaceAll(" ", "_")
					// 	} else {
					// 		sf6Data[charName]["moves"]["normal"][moveData["moveName"]].dustloopKey = moveData["moveName"].replaceAll(" ", "_")
					// 	}			
					// }
			}


			// check if this character has a special state that needs to be parsed
			Object.keys(rawData).forEach(keyName => {
				if (keyName.includes(charName) && keyName !== `${charName}Normal` && keyName !== `${charName}Stats`) {
					let workingDataSpecialState = rawData[keyName];
					const stateName = keyName.substring(charName.length);
					errState = stateName;

					sf6Data[charName]["moves"][stateName] = {};

					for (var move in workingDataSpecialState) {
						var moveData = workingDataSpecialState[move];
						errMove = moveData["moveName"]

						//creates the new move entry
						sf6Data[charName]["moves"][stateName][moveData["moveName"]] = {};
						
						for (heading in moveData) {
							errEntry = heading;
		
							if (!ignoreRows.includes(heading)) {
								var preFormatData = moveData[heading];
								//before starting, check that none of the entites start with whitespace. If it does, throw an error
								if (preFormatData[0] === " ") {
									throw `whitespace detected in the first character of this cell for ${charName}: ${moveData["moveName"]}'s ${heading}`;
								}
								// First we skip over any blank entries. These can be accomidated for in the app, and significantly cut down on JSON space
								if (preFormatData !== "" && preFormatData !== "~" && preFormatData !== "-" && preFormatData !== "false") {
										// Next we check whether the entry is a number or not. If it is, we go to 'else' and Number() it
										if (isNaN(preFormatData) || heading === "numCmd") {
											if (preFormatData === "true" || preFormatData[0] === "{" || (preFormatData[0] === "[" && !heading.includes("Cmd") && !heading.includes("Name"))) {
												// If entry is a (true) Boolean, or a JSON or a list, we need to parse it	
												sf6Data[charName]["moves"][stateName][moveData["moveName"]][heading] = JSON.parse(preFormatData)
											} else if (heading === "i" && preFormatData === "r") {
												//if the index column says r, we should make it the normal's index instead
												sf6Data[charName]["moves"][stateName][moveData["moveName"]][heading] = sf6Data[charName]["moves"]["normal"][moveData["moveName"]][heading];
											} else if (heading === "numCmd") {
												// some numCmd entries are just numbers. This breaks things. We need to make sure it's a string
												sf6Data[charName]["moves"][stateName][moveData["moveName"]][heading] = preFormatData.toString();
											} else {
												sf6Data[charName]["moves"][stateName][moveData["moveName"]][heading] = preFormatData;												
											}
										} else {
												sf6Data[charName]["moves"][stateName][moveData["moveName"]][heading] = Number(preFormatData);
										}
								}
						}
						}

						// if (!sf6Data[charName]["moves"][stateName][moveData["moveName"]].dustloopKey) {
						
						// 	if (moveData["moveName"] === "Throw") {
						// 		sf6Data[charName]["moves"][stateName][moveData["moveName"]].dustloopKey = "Ground_Throw"
						// 	} else if (
						// 		moveData["moveName"].substring(0, 2) === "P " ||
						// 		moveData["moveName"].substring(0, 2) === "K " ||
						// 		moveData["moveName"].substring(0, 2) === "S " ||
						// 		moveData["moveName"].substring(0, 2) === "H " ||
						// 		moveData["moveName"].substring(0, 2) === "D "
						// 	) {
						// 		sf6Data[charName]["moves"][stateName][moveData["moveName"]].dustloopKey = moveData["moveName"].slice(2).replaceAll(" ", "_")
						// 		if (moveData["moveName"].includes(" (air)")) {
						// 			sf6Data[charName]["moves"][stateName][moveData["moveName"]].dustloopKey = moveData["moveName"].replaceAll(" (air)", "").replaceAll(" ", "_")
						// 		}
						// 	} else if (moveData["moveName"].includes(" (air)")) {
						// 		sf6Data[charName]["moves"][stateName][moveData["moveName"]].dustloopKey = moveData["moveName"].replaceAll(" (air)", "").replaceAll(" ", "_")
						// 	} else {
						// 		sf6Data[charName]["moves"][stateName][moveData["moveName"]].dustloopKey = moveData["moveName"].replaceAll(" ", "_")
						// 	}			
						// }
				}




				}
			})
	
			//stats
			errState = "Stats"
			if (typeof rawData[charName + "Stats"] !== "undefined") {
					var workingDataStats = rawData[charName + "Stats"];
					
					//incorporate the sheet stats
					for (entry in workingDataStats) {
							sf6Data[charName]["stats"][workingDataStats[entry]["name"]] = workingDataStats[entry]["stat"].toString();
					}
					
					//create a fastest normal stat
					let currentFastestNormal = [10, "moveName"];
					
          rawData[charName + "Normal"].map(moveData => {
            if (moveData.moveType === "normal" && moveData.followUp !== "true" && moveData.airmove !== "true" && !moveData.movesList && parseInt(moveData.startup) < currentFastestNormal[0]) {
              currentFastestNormal = [moveData.startup, moveData.moveName]
            }
          })
					
					sf6Data[charName]["stats"]["fastestNormal"] = `${currentFastestNormal[0].toString()}f`;

			}
		}
		console.log(sf6Data)
    return [
			"success",
			sf6Data
		];

	} catch(err) {
		console.log(err)
		return [
			"error",
			{ errChar, errState, errMove, errEntry }
		];
	}
	



    

}

onmessage = function(e) {
	const fatFormattedJSON = convertSheetJSONtoFATJSONSF6(e.data);
	postMessage(fatFormattedJSON)
}
