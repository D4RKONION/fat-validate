# FAT Validate

Turn FAT Sheet into FAT JSON. Most of the magic happens in ./public/workers. Remember, new characters DO need to be specified in FAT Validate in order for them to make it through to FAT.

## Adding a new character

Add the character to the characterList at the top of the worker file for that game. Bump the version number in ./src/js/Home.jsx

## Deployment

Don't forget to bump the version number in Home.jsx, then do `npm run build`


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.